public class PowerSocket { //розетка с двумя выводами
    private int voltage;
    private int output;

    public PowerSocket(int voltage) {
        this.voltage = voltage;
        this.output = 2;
    }

    public int getVoltage() {
        return voltage;
    }

    public int getOutput() {
        return output;
    }

    public boolean compatibility(Plug plug) {
        return (this.voltage == plug.getVoltage() &&
                this.output == plug.getOutput());
    }
}
