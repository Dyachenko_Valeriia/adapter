public class Adapter extends Plug {
    private BritishPlug britishPlug;

    public Adapter(int voltage, BritishPlug britishPlug) {
        super(voltage);
        this.britishPlug = britishPlug;
    }
}
