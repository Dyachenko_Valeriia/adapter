public class Plug { //Вилка с двумя контактами
    private int voltage;
    private int output;

    public Plug(int voltage) {
        this.voltage = voltage;
        this.output = 2;
    }

    public int getVoltage() {
        return voltage;
    }

    public int getOutput() {
        return output;
    }
}
