public class BritishPlug { //Вилка с тремя контактами
    private int voltage;
    private int output;

    public BritishPlug(int voltage) {
        this.voltage = voltage;
        this.output = 3;
    }

    public int getVoltage() {
        return voltage;
    }

    public int getOutput() {
        return output;
    }

    public void getPlug() {
        this.output = 2;
    }
}
