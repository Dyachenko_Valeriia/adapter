import org.junit.Test;
import static junit.framework.TestCase.assertTrue;

public class TestAdapter {
    @Test
    public void testAdapterPlug() { //Вилка с двумя контактами совместна с розеткой с двумя выходами
        PowerSocket powerSocket = new PowerSocket(220);
        Plug plug = new Plug(220);
        assertTrue(powerSocket.compatibility(plug));
    }

    @Test
    public void testAdapterBritishPlug() {//Вилка с тремя контактами совместна с розеткой с двумя выходами через адаптер
        PowerSocket powerSocket = new PowerSocket(220);
        BritishPlug britishplug = new BritishPlug(220);
        Adapter adapter = new Adapter(220, britishplug);
        assertTrue(powerSocket.compatibility(adapter));
    }
}
